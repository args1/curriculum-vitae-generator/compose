FROM odoo:14.0

USER root

RUN apt update && apt install -y \
  locales

RUN echo fr_FR.UTF-8 UTF-8 >> /etc/locale.gen && locale-gen

USER odoo
